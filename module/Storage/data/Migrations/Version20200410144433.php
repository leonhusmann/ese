<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200410144433 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE addresses (id INT UNSIGNED AUTO_INCREMENT NOT NULL, street VARCHAR(200) NOT NULL, streetnumber VARCHAR(50) NOT NULL, zipcode VARCHAR(50) NOT NULL, city VARCHAR(200) NOT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE facilities ADD address_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE facilities ADD CONSTRAINT FK_ADE885D5F5B7AF75 FOREIGN KEY (address_id) REFERENCES addresses (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_ADE885D5F5B7AF75 ON facilities (address_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE facilities DROP FOREIGN KEY FK_ADE885D5F5B7AF75');
        $this->addSql('DROP TABLE addresses');
        $this->addSql('DROP INDEX UNIQ_ADE885D5F5B7AF75 ON facilities');
        $this->addSql('ALTER TABLE facilities DROP address_id');
    }
}
