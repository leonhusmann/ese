<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200412095845 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tags_taggroups');
        $this->addSql('ALTER TABLE tags ADD taggroup_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE tags ADD CONSTRAINT FK_6FBC9426B82D1590 FOREIGN KEY (taggroup_id) REFERENCES taggroups (id)');
        $this->addSql('CREATE INDEX IDX_6FBC9426B82D1590 ON tags (taggroup_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tags_taggroups (tag_id INT UNSIGNED NOT NULL, taggroup_id INT UNSIGNED NOT NULL, INDEX IDX_3CB8F2B6B82D1590 (taggroup_id), INDEX IDX_3CB8F2B6BAD26311 (tag_id), PRIMARY KEY(tag_id, taggroup_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE tags_taggroups ADD CONSTRAINT FK_3CB8F2B6B82D1590 FOREIGN KEY (taggroup_id) REFERENCES taggroups (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tags_taggroups ADD CONSTRAINT FK_3CB8F2B6BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tags DROP FOREIGN KEY FK_6FBC9426B82D1590');
        $this->addSql('DROP INDEX IDX_6FBC9426B82D1590 ON tags');
        $this->addSql('ALTER TABLE tags DROP taggroup_id');
    }
}
