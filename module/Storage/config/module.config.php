<?php

namespace Storage;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Storage\Migrations\Factory\DiffCommandFactory;
use Storage\Migrations\Factory\GenerateCommandFactory;

return [
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ]
    ],
    'service_manager' => [
        'factories' => [
            'doctrine.migrations_cmd.generate' => GenerateCommandFactory::class,
            'doctrine.migrations_cmd.diff' => DiffCommandFactory::class,
        ],
    ],
];
