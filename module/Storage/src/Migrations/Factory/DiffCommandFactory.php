<?php

namespace Storage\Migrations\Factory;

use Doctrine\DBAL\Migrations\Configuration\Configuration;
use Interop\Container\ContainerInterface;
use Storage\Migrations\Command\DiffCommand;

class DiffCommandFactory
{
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return DiffCommand
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): DiffCommand {
        /** @var Configuration $configuration */
        $configuration = $container->get('doctrine.migrations_configuration.orm_default');
        $command = new DiffCommand();
        $command->setMigrationConfiguration($configuration);

        return $command;
    }
}