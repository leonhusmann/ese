<?php

namespace Storage\Migrations\Command;

class GenerateCommand extends \Doctrine\Migrations\Tools\Console\Command\GenerateCommand
{
    protected static $defaultName = 'migrations:generate';

    protected function getTemplate(): string
    {
        return file_get_contents(__DIR__ . '/MigrationGenerateTemplate.tmpl');
    }
}