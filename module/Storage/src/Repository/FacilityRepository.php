<?php

declare(strict_types=1);

namespace Storage\Repository;

use Application\Model\Search\SearchModel;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;
use Storage\Entity\Facility;

class FacilityRepository extends EntityRepository
{
    public function save(Facility $facility): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($facility);
        $entityManager->flush();
    }

    public function delete(Facility $facility): void
    {
        $entityManager = $this->getEntityManager();

        $sql = 'DELETE FROM facility_tags WHERE facility_id = :facilityId';
        $connection = $entityManager->getConnection();

        $connection->executeQuery(
            $sql,
            [
                'facilityId' => $facility->getId(),
            ],
            [
                'facilityId' => Types::INTEGER
            ],
        );

        $entityManager->remove($facility);
        $entityManager->flush($facility);
    }
    /**
     * @return Facility[]
     */
    public function findBySearchModel(SearchModel $searchModel): array
    {
        $joins = [];
        $where = [];
        $params = [];
        $types = [];

        if (!empty($searchModel->getFacilityName())) {
            $where[] = ' AND f.name LIKE :facilityName';
            $params['facilityName'] = sprintf('%%%s%%', $searchModel->getFacilityName());
            $types['facilityName'] = Types::STRING;
        }

        if (!empty($searchModel->getTagGroups())) {
            foreach ($searchModel->getTagGroups() as $key => $tagGroup) {
                $tagsPlaceholder = sprintf('tags%d', $key);
                $joins[] = sprintf(' INNER JOIN facility_tags ft%d ON ft%d.facility_id = f.id', $key, $key);
                $where[] = sprintf(' AND ft%d.tag_id IN (:%s)', $key, $tagsPlaceholder);
                $params[$tagsPlaceholder] = $tagGroup->getTagIdsAsArray();
                $types[$tagsPlaceholder] = Types::SIMPLE_ARRAY;
            }
        }

        $connection = $this->getEntityManager()->getConnection();

        $result = $connection->executeQuery(
            $this->generateSqlQuery($joins, $where),
            $params,
            $types
        )->fetchAll(FetchMode::ASSOCIATIVE);

        if (count($result) === 0) {
            return [];
        }

        $ids = [];
        foreach ($result as $resultItem) {
            $ids[] = $resultItem['id'];
        }

        return $this->findBy([
            'id' => $ids
        ]);
    }

    private function generateSqlQuery(array $joins, array $where): string
    {
        $sql = 'SELECT f.id FROM facilities f';

        foreach (array_unique($joins) as $join) {
            $sql.= $join;
        }
        if (count($where) > 0) {
            $sql.= ' WHERE 1=1';
        }
        foreach ($where as $whereStatement) {
            $sql.= $whereStatement;
        }

        return $sql;
    }
}