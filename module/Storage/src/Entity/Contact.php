<?php

declare(strict_types=1);

namespace Storage\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="contacts",
 *     options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"}
 * )
 * @ORM\Entity(repositoryClass="Storage\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @var int|null
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Facility", inversedBy="contacts")
     * @ORM\JoinColumn(name="facility_id", referencedColumnName="id")
     */
    private Facility $facility;

    /**
     * @var string
     * @ORM\Column(name="firstname", type="string", length=200)
     */
    private string $firstname;

    /**
     * @var string
     * @ORM\Column(name="lastname", type="string", length=200)
     */
    private string $lastname;

    /**
     * @var ?string
     * @ORM\Column(name="position", type="string", length=200)
     */
    private ?string $position;

    /**
     * @var ?string
     * @ORM\Column(name="phonenumber", type="string", length=200)
     */
    private ?string $phoneNumber;

    /**
     * @var ?string
     * @ORM\Column(name="mobilenumber", type="string", length=200)
     */
    private ?string $mobilenumber;

    /**
     * @var ?string
     * @ORM\Column(name="faxnumber", type="string", length=200)
     */
    private ?string $faxnumber;

    /**
     * @var ?string
     * @ORM\Column(name="email", type="string", length=200)
     */
    private ?string $email;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private \DateTime $createdAt;

    /**
     * @var ?\DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private ?\DateTime $updatedAt = null;

    public function __construct(Facility $facility)
    {
        $this->id = null;
        $this->facility = $facility;
        $this->createdAt = new \DateTime('now', new \DateTimeZone('utc'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFacility(): Facility
    {
        return $this->facility;
    }

    public function setFacility(Facility $facility): self
    {
        $this->facility = $facility;

        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition($position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber($phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getMobilenumber(): ?string
    {
        return $this->mobilenumber;
    }

    public function setMobilenumber(?string $mobilenumber): self
    {
        $this->mobilenumber = $mobilenumber;

        return $this;
    }

    public function getFaxnumber(): ?string
    {
        return $this->faxnumber;
    }

    public function setFaxnumber($faxnumber): self
    {
        $this->faxnumber = $faxnumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function hasPhoneNumber(): bool
    {
        return !empty($this->getPhoneNumber());
    }

    public function hasMobileNumber(): bool
    {
        return !empty($this->getMobilenumber());
    }

    public function hasFaxnumber(): bool
    {
        return !empty($this->getFaxnumber());
    }

    public function hasEmail(): bool
    {
        return !empty($this->getEmail());
    }
}