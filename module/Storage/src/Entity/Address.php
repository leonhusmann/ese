<?php

declare(strict_types=1);

namespace Storage\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="addresses",
 *     options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"}
 * )
 * @ORM\Entity(repositoryClass="Storage\Repository\AddressRepository")
 */
class Address
{
    /**
     * @var int|null
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     * @ORM\Column(name="street", type="string", length=200)
     */
    private string $street;

    /**
     * @var string
     * @ORM\Column(name="streetnumber", type="string", length=50)
     */
    private string $streetnumber;

    /**
     * @var string
     * @ORM\Column(name="zipcode", type="string", length=50)
     */
    private string $zipcode;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=200)
     */
    private string $city;

    /**
     * @var ?float
     * @ORM\Column(name="latitude", type="float", precision=16, scale=12, length=200, nullable=true)
     */
    private ?float $latitude = null;

    /**
     * @var ?float
     * @ORM\Column(name="longitude", type="float", precision=16, scale=12, length=200, nullable=true)
     */
    private ?float $longitude = null;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private \DateTime $createdAt;

    /**
     * @var ?\DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private ?\DateTime $updatedAt = null;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now', new \DateTimeZone('utc'));
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getStreetnumber(): string
    {
        return $this->streetnumber;
    }

    public function setStreetnumber(string $streetnumber): self
    {
        $this->streetnumber = $streetnumber;

        return $this;
    }

    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getAddressAsString(): string
    {
        return sprintf(
            '%s %s, %s %s',
            $this->getStreet(),
            $this->getStreetnumber(),
            $this->getZipcode(),
            $this->getCity()
        );
    }

}