<?php

declare(strict_types=1);

namespace Storage\Entity;

use Application\Model\OpeningTimes\OpeningTimesCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="facilities",
 *     options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"}
 * )
 * @ORM\Entity(repositoryClass="Storage\Repository\FacilityRepository")
 */
class Facility
{
    /**
     * @var int|null
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=200)
     */
    private string $name;

    /**
     * @var ?string
     * @ORM\Column(name="description", type="text", length=60000, nullable=true)
     */
    private ?string $description = null;

    /**
     * @ORM\OneToOne(targetEntity="Address", orphanRemoval=true, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", onDelete="cascade")
     */
    private Address $address;

    /**
     * @ORM\ManyToMany(targetEntity="Service", cascade={"remove"})
     * @ORM\JoinTable(name="facility_services",
     *      joinColumns={@ORM\JoinColumn(name="facility_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id", unique=true, onDelete="cascade")}
     * )
     */
    private Collection $services;

    /**
     * @ORM\OneToMany(targetEntity="OpeningTime", mappedBy="facility", cascade={"persist"})
     */
    private Collection $openingTimes;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", cascade={"remove"})
     * @ORM\JoinTable(name="facility_tags",
     *      joinColumns={@ORM\JoinColumn(name="facility_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    private Collection $tags;

    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="facility", cascade={"persist"})
     */
    private Collection $contacts;

    /**
     * @var ?string
     * @ORM\Column(name="phone_number", type="string", length=200, nullable=true)
     */
    private ?string $phoneNumber = null;

    /**
     * @var ?string
     * @ORM\Column(name="mobile_number", type="string", length=200, nullable=true)
     */
    private ?string $mobileNumber = null;

    /**
     * @var ?string
     * @ORM\Column(name="fax_number", type="string", length=200, nullable=true)
     */

    private ?string $faxNumber = null;

    /**
     * @var ?string
     * @ORM\Column(name="website", type="string", length=200, nullable=true)
     */
    private ?string $website = null;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private \DateTime $createdAt;

    /**
     * @var \DateTime|null
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private \DateTime $updatedAt;

    public function __construct(
        string $name
    ) {
        $this->name = $name;
        $this->openingTimes = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(bool $removeTags = false): ?string
    {
        if ($removeTags) {
            return strip_tags($this->description);
        }

        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Service[]
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        $this->services->add($service);

        return $this;
    }

    public function getOpeningTimes(): OpeningTimesCollection
    {
        return new OpeningTimesCollection($this->openingTimes->toArray());
    }

    public function addOpeningTime(OpeningTime $openingTime): self
    {
        $this->openingTimes->add($openingTime);

        return $this;
    }

    public function clearOpeningTimes(): self
    {
        $this->openingTimes->clear();

        return $this;
    }

    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
        }

        return $this;
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        $this->tags->add($tag);

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getMobileNumber(): ?string
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber(?string $mobileNumber): self
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    public function getWebsite(bool $includePrefix = true): ?string
    {
        return $includePrefix ? 'https://' . $this->website : $this->website;
    }

    public function setWebsite($website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function hasPhoneNumber(): bool
    {
        return !empty($this->getPhoneNumber());
    }

    public function hasMobileNumber(): bool
    {
        return !empty($this->getMobileNumber());
    }

    public function hasWebsite(): bool
    {
        return !empty($this->getWebsite());
    }

    public function hasFaxNumber(): bool
    {
        return !empty($this->getFaxNumber());
    }

    public function hasDescription(): bool
    {
        return !empty($this->getDescription());
    }
}