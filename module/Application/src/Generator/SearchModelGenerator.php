<?php

declare(strict_types=1);

namespace Application\Generator;

use Application\Model\Search\SearchModel;

class SearchModelGenerator
{
    public function generateFromRequest($queryParams): SearchModel
    {
        $searchModel = new SearchModel();

        if ($queryParams['filter']['name'] !== null) {
            $searchModel->setFacilityName($queryParams['filter']['name']);
        }

        if ($queryParams['filter']['tagGroups'] !== null) {
            foreach ($queryParams['filter']['tagGroups'] as $tagGroupId => $tagGroup) {
                if ($queryParams['filter']['tagGroups'][$tagGroupId]) {
                    $tags = [];
                    foreach ($queryParams['filter']['tagGroups'][$tagGroupId] as $tagId => $checked) {
                        if ($checked) {
                            $tags[] = $tagId;
                        }
                    }
                    if (!empty($tags)) {
                        $searchModel->addTagGroupAndTags($tagGroupId, $tags);
                    }
                }
            }
        }

        return $searchModel;
    }
}