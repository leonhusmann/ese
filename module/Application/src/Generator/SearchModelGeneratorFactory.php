<?php

declare(strict_types=1);

namespace Application\Generator;

use Psr\Container\ContainerInterface;

class SearchModelGeneratorFactory
{
    public function __invoke(ContainerInterface $container): SearchModelGenerator
    {
        return new SearchModelGenerator();
    }
}