<?php

declare(strict_types=1);

namespace Application\Model\Search\Struct;

class TagGroup
{
    private int $id;
    private array $tags = [];

    public function __construct(int $tagGroupId, array $tags)
    {
        $this->id = $tagGroupId;
        $this->tags = $tags;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @return int[]
     */
    public function getTagIdsAsArray(): array
    {
        return array_map(function(Tag $tag) {
            return $tag->getId();
        }, $this->getTags());
    }
}