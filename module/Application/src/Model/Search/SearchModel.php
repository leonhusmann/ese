<?php

declare(strict_types=1);

namespace Application\Model\Search;

use Application\Model\Search\Struct\Tag;
use Application\Model\Search\Struct\TagGroup;

class SearchModel
{
    private ?string $facilityName = null;
    /** @var TagGroup[] */
    private array $tagGroups = [];

    public function getFacilityName(): ?string
    {
        return $this->facilityName;
    }

    public function setFacilityName(?string $facilityName): self
    {
        $this->facilityName = $facilityName;

        return $this;
    }

    /**
     * @return TagGroup[]
     */
    public function getTagGroups(): array
    {
        return $this->tagGroups;
    }

    /**
     * @param int[] $tagIds
     */
    public function addTagGroupAndTags(int $tagGroupId, array $tagIds): self
    {
        $tags = [];
        foreach ($tagIds as $tagId) {
            $tags[] = new Tag($tagId);
        }

        $this->tagGroups[] = new TagGroup($tagGroupId, $tags);

        return $this;
    }
}