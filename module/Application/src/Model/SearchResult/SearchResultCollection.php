<?php

declare(strict_types=1);

namespace Application\Model\SearchResult;

use Doctrine\Common\Collections\ArrayCollection;

class SearchResultCollection extends ArrayCollection
{
}