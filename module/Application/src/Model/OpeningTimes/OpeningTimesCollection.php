<?php

declare(strict_types=1);

namespace Application\Model\OpeningTimes;

use Doctrine\Common\Collections\ArrayCollection;
use Storage\Entity\OpeningTime;

class OpeningTimesCollection extends ArrayCollection
{
    public const PERIOD_AM = 'AM';
    public const PERIOD_PM = 'PM';

    public const CURRENTLY_OPEN = 'CURRENTLY_OPEN';
    public const CLOSES_SOON = 'CLOSES_SOON';
    public const CLOSED = 'CLOSED';

    public function filterByDayOfWeek(int $dayOfWeek): self
    {
        return $this->filter(function(OpeningTime $openingTime) use ($dayOfWeek) {
            return $openingTime->getDayOfWeek() === $dayOfWeek;
        });
    }

    public function filterByPeriod(string $period): self
    {
        return $this->filter(function(OpeningTime $openingTime) use ($period) {
            return $openingTime->getPeriod() === $period;
        });
    }

    public function findByDayOfWeekAndPeriod(int $dayOfWeek, string $period): ?OpeningTime
    {
        $openingTimes = clone $this;
        $openingTimes = $openingTimes->filterByDayOfWeek($dayOfWeek)->filterByPeriod($period);

        return $openingTimes->count() === 1 ? $openingTimes->first() : null;
    }

    public function getOpeningStatus(): string
    {
        return $this->closesSoon() ? self::CLOSES_SOON : ($this->isCurrentlyOpen() ? self::CURRENTLY_OPEN : self::CLOSED);
    }

    public function isCurrentlyOpen(): bool
    {
        return $this->getActiveOpeningTimeEntity() ? true : false;
    }

    public function closesSoon(): bool
    {
        $activeOpeningTime = $this->getActiveOpeningTimeEntity();
        if (!$activeOpeningTime) {
            return false;
        }

        $currentDateTime = new \DateTime('now', new \DateTimeZone('Europe/Berlin'));
        $currentDateTime = $currentDateTime->modify('+30 minute');

        return $currentDateTime->format('H:i') >= $activeOpeningTime->getEndTime()->format('H:i');
    }

    private function getActiveOpeningTimeEntity(): ?OpeningTime
    {
        $currentDateTime = new \DateTime('now', new \DateTimeZone('Europe/Berlin'));
        $currentDayOfWeek = $currentDateTime->format('N') % 7;

        /** @var OpeningTime $openingTime */
        foreach ($this->filterByDayOfWeek($currentDayOfWeek) as $openingTime) {
            if ($openingTime->getStartTime()->format('H:i') <= $currentDateTime->format('H:i')
                && $openingTime->getEndTime()->format('H:i') > $currentDateTime->format('H:i')) {
                return $openingTime;
            }
        }

        return null;
    }

    public static function translateOpenningStatus(string $openingStatus): string
    {
        switch ($openingStatus) {
            case self::CURRENTLY_OPEN: return 'Aktuell geöffnet';
            case self::CLOSES_SOON: return 'Schließt demnächst';
            default: return 'Aktuell geschlossen';
        }
    }

    public static function translateDayOfWeek(int $dayOfWeek): string
    {
        switch ($dayOfWeek) {
            case 1: return 'Montag';
            case 2: return 'Dienstag';
            case 3: return 'Mittwoch';
            case 4: return 'Donnerstag';
            case 5: return 'Freitag';
            case 6: return 'Samstag';
            default: return 'Sonntag';
        }
    }
}