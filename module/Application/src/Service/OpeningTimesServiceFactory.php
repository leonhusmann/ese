<?php

namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Storage\Entity\OpeningTime;
use Storage\Repository\OpeningTimesRepository;

class OpeningTimesServiceFactory
{
    public function __invoke(ContainerInterface $container): OpeningTimesService
    {
        $entityManager = $container->get(EntityManager::class);
        /** @var OpeningTimesRepository $openingTimesRepository */
        $openingTimesRepository = $entityManager->getRepository(OpeningTime::class);

        return new OpeningTimesService(
            $openingTimesRepository
        );
    }
}