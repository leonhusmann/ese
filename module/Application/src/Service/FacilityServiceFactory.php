<?php

declare(strict_types=1);

namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Storage\Entity\Facility;
use Storage\Repository\FacilityRepository;

class FacilityServiceFactory
{
    public function __invoke(ContainerInterface $container): FacilityService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var FacilityRepository $facilityRepository */
        $facilityRepository = $entityManager->getRepository(Facility::class);

        return new FacilityService(
            $facilityRepository
        );
    }
}