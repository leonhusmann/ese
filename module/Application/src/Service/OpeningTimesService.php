<?php


namespace Application\Service;

use Storage\Repository\OpeningTimesRepository;

class OpeningTimesService
{
    private OpeningTimesRepository $openingTimesRepository;

    public function __construct(OpeningTimesRepository $openingTimesRepository)
    {
        $this->openingTimesRepository = $openingTimesRepository;
    }

    public function removeOpeningTimes(int $facilityId): bool
    {
        return $this->openingTimesRepository->removeByFacilityId($facilityId);
    }
}