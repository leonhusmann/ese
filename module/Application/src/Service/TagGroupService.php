<?php

declare(strict_types=1);

namespace Application\Service;

use Storage\Entity\TagGroup;
use Storage\Repository\TagGroupRepository;
use Storage\Repository\TagRepository;

class TagGroupService
{
    private TagGroupRepository $tagGroupRepository;
    private TagRepository $tagRepository;

    public function __construct(
        TagGroupRepository $tagGroupRepository,
        TagRepository $tagRepository
    ) {
        $this->tagGroupRepository = $tagGroupRepository;
        $this->tagRepository = $tagRepository;
    }

    public function getTagGroups(): array
    {
        /** @var TagGroup[] $tagGroups */
        $tagGroups = $this->tagGroupRepository->findAll();

        return $tagGroups;
    }

    public function getTagGroup(int $tagGroupId): TagGroup
    {
        /** @var TagGroup $tagGroup */
        $tagGroup = $this->tagGroupRepository->find($tagGroupId);

        return $tagGroup;
    }

    public function save(TagGroup $tagGroup): void
    {
        $this->tagGroupRepository->save($tagGroup);
    }

    public function delete(TagGroup $tagGroup): void
    {
        foreach ($tagGroup->getTags() as $tag) {
            $this->tagRepository->delete($tag);
        }

        $this->tagGroupRepository->delete($tagGroup);
    }
}