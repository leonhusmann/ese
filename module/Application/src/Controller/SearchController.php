<?php

declare(strict_types=1);

namespace Application\Controller;

use Application\Generator\SearchModelGenerator;
use Application\Service\SearchService;
use Application\Service\TagGroupService;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class SearchController extends AbstractActionController
{
    private SearchModelGenerator $searchModelGenerator;
    private SearchService $searchService;
    private TagGroupService $tagGroupService;

    public function __construct(
        SearchModelGenerator $searchModelGenerator,
        SearchService $searchService,
        TagGroupService $tagGroupService
    ) {
        $this->searchModelGenerator = $searchModelGenerator;
        $this->searchService = $searchService;
        $this->tagGroupService = $tagGroupService;
    }

    public function searchAction(): ViewModel
    {
        $filter = $this->params()->fromQuery();

        $tagGroups = $this->tagGroupService->getTagGroups();

        $results = $this->searchService->search(
            $this->searchModelGenerator->generateFromRequest($filter)
        );

        return new ViewModel([
            'searchResults' => $results,
            'tagGroups' => $tagGroups,
            'filter' => $filter['filter'],
        ]);
    }

    public function detailsAction(): ViewModel
    {
        $facilityId = $this->params()->fromRoute('id');

        if ($facilityId === null) {
            throw new \InvalidArgumentException(
                'Missing facilityId',
                8712687162378
            );
        }

        $facility = $this->searchService->getFacilityById((int) $facilityId);

        return new ViewModel([
            'facility' => $facility,
        ]);
    }
}