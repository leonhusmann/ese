<?php

declare(strict_types=1);

namespace Application\Controller;

use Application\Generator\SearchModelGenerator;
use Application\Service\SearchService;
use Application\Service\TagGroupService;
use Psr\Container\ContainerInterface;

class SearchControllerFactory
{
    public function __invoke(ContainerInterface $container): SearchController
    {
        return new SearchController(
            $container->get(SearchModelGenerator::class),
            $container->get(SearchService::class),
            $container->get(TagGroupService::class)
        );
    }
}