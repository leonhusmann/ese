<?php

declare(strict_types=1);

namespace Application;

use Application\Controller\SearchController;
use Application\Controller\SearchControllerFactory;
use Application\Generator\SearchModelGenerator;
use Application\Generator\SearchModelGeneratorFactory;
use Application\Service\ContactService;
use Application\Service\ContactServiceFactory;
use Application\Service\FacilityService;
use Application\Service\FacilityServiceFactory;
use Application\Service\OpeningTimesService;
use Application\Service\OpeningTimesServiceFactory;
use Application\Service\SearchService;
use Application\Service\SearchServiceFactory;
use Application\Service\TagGroupService;
use Application\Service\TagGroupServiceFactory;
use Application\Service\TagService;
use Application\Service\TagServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'search' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => SearchController::class,
                        'action' => 'search',
                    ],
                ],
            ],
            'details' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/details/:id',
                    'defaults' => [
                        'controller' => SearchController::class,
                        'action' => 'details',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            SearchController::class => SearchControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            ContactService::class => ContactServiceFactory::class,
            FacilityService::class => FacilityServiceFactory::class,
            OpeningTimesService::class => OpeningTimesServiceFactory::class,
            SearchModelGenerator::class => SearchModelGeneratorFactory::class,
            SearchService::class => SearchServiceFactory::class,
            TagGroupService::class => TagGroupServiceFactory::class,
            TagService::class => TagServiceFactory::class,
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ]
];
