<?php

declare(strict_types=1);

namespace Administration\Controller;

use Laminas\Authentication\AuthenticationService;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\MvcEvent;

abstract class AbstractAuthenticationController extends AbstractActionController
{
    private AuthenticationService $authenticationService;

    public function __construct(
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
    }

    public function onDispatch(MvcEvent $e)
    {
        if (!$this->authenticationService->hasIdentity()) {
            $this->redirect()->toRoute('administration/login');
        }

        return parent::onDispatch($e);
    }
}