<?php

declare(strict_types=1);

namespace Administration\Controller;

use Administration\Service\AuthManagerService;
use Laminas\Authentication\AuthenticationService;
use Psr\Container\ContainerInterface;

class AuthControllerFactory
{
    public function __invoke(ContainerInterface $container): AuthController
    {
        return new AuthController(
            $container->get(AuthManagerService::class),
            $container->get(AuthenticationService::class)
        );
    }
}