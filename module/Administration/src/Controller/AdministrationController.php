<?php

declare(strict_types=1);

namespace Administration\Controller;

use Application\Service\ContactService;
use Application\Service\FacilityService;
use Application\Service\OpeningTimesService;
use Application\Service\SearchService;
use Application\Service\TagGroupService;
use Application\Service\TagService;
use Laminas\Authentication\AuthenticationService;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Storage\Entity\Address;
use Storage\Entity\Contact;
use Storage\Entity\Facility;
use Storage\Entity\OpeningTime;
use Storage\Entity\Tag;
use Storage\Entity\TagGroup;

class AdministrationController extends AbstractAuthenticationController
{
    private SearchService $searchService;
    private TagGroupService $tagGroupService;
    private TagService $tagService;
    private FacilityService $facilityService;
    private ContactService $contactService;
    private OpeningTimesService $openingTimesService;

    public function __construct(
        AuthenticationService $authenticationService,
        SearchService $searchService,
        TagGroupService $tagGroupService,
        TagService $tagService,
        FacilityService $facilityService,
        ContactService $contactService,
        OpeningTimesService $openingTimesService
    ) {
        parent::__construct($authenticationService);

        $this->searchService = $searchService;
        $this->tagGroupService = $tagGroupService;
        $this->tagService = $tagService;
        $this->facilityService = $facilityService;
        $this->contactService = $contactService;
        $this->openingTimesService = $openingTimesService;
    }

    //--------------------------------------------------------------------------------------------------
    // Facilities
    //--------------------------------------------------------------------------------------------------

    public function indexAction(): ViewModel
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $facilities = $this->searchService->getFacilities();

        return new ViewModel([
            'facilities' => $facilities,
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function facilitiesEditAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $facilityId = $this->params()->fromRoute('facilityId');

        /** @var Facility $facility */
        $facility = $this->facilityService->getFacility((int) $facilityId);
        /** @var TagGroup[] $tagGroups */
        $tagGroups = $this->tagGroupService->getTagGroups();

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');
            $formData['tags'] = $formData['tags'] ?? [];

            $facility->setName($formData['name']);
            $facility->setPhoneNumber($formData['phone']);
            $facility->setMobileNumber($formData['mobile']);
            $facility->setFaxNumber($formData['fax']);
            $facility->setDescription($formData['description']);
            $facility->setWebsite($formData['website']);
            $facility->getAddress()->setStreet($formData['address']['street']);
            $facility->getAddress()->setStreetnumber($formData['address']['streetnumber']);
            $facility->getAddress()->setZipcode($formData['address']['zipcode']);
            $facility->getAddress()->setCity($formData['address']['city']);
            $facility->getAddress()->setLatitude($formData['address']['latitude'] ?? null);
            $facility->getAddress()->setLongitude($formData['address']['longitude'] ?? null);
            $facility->getAddress()->setUpdatedAt(new \DateTime('now'));

            $this->openingTimesService->removeOpeningTimes((int) $facilityId);

            foreach ($formData['ot'] as $dayOfWeek => $dayInfo) {
                foreach ($dayInfo as $period => $periodTimes) {
                    if (!empty($periodTimes['from']) && !empty($periodTimes['to'])) {
                        $openingTime = new OpeningTime(
                            $facility,
                            $dayOfWeek,
                            new \DateTime($periodTimes['from'], new \DateTimeZone('Europe/Berlin')),
                            new \DateTime($periodTimes['to'], new \DateTimeZone('Europe/Berlin')),
                            $period
                        );
                        $facility->addOpeningTime($openingTime);
                    }
                }
            }

            foreach ($formData['tags'] as $tagId => $checked) {
                $tag = $this->tagService->getTag($tagId);
                if ((bool) $checked === true) {
                    if (!$facility->getTags()->contains($tag)) {
                        $facility->getTags()->add($tag);
                    }
                } else {
                    $key = $facility->getTags()->indexOf($tag);
                    if ($key) {
                        $facility->getTags()->remove($key);
                    }
                }
            }

            $facility->setUpdatedAt(new \DateTime('now'));
            $this->facilityService->save($facility);

            $this->redirect()->toRoute('administration');
        }

        return new ViewModel([
            'facility' => $facility,
            'tagGroups' => $tagGroups,
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function facilitiesAddAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        /** @var TagGroup[] $tagGroups */
        $tagGroups = $this->tagGroupService->getTagGroups();

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');
            $formData['tags'] = $formData['tags'] ?? [];

            $facility = new Facility($formData['name']);
            $facility->setPhoneNumber($formData['phone']);
            $facility->setMobileNumber($formData['mobile']);
            $facility->setFaxNumber($formData['fax']);
            $facility->setDescription($formData['description']);
            $facility->setWebsite($formData['website']);

            $address = new Address();
            $address->setStreet($formData['address']['street']);
            $address->setStreetnumber($formData['address']['streetnumber']);
            $address->setZipcode($formData['address']['zipcode']);
            $address->setCity($formData['address']['city']);
            $address->setLatitude($formData['address']['latitude'] ?? 0.00);
            $address->setLongitude($formData['address']['longitude'] ?? 0.00);
            $facility->setAddress($address);

            foreach ($formData['tags'] as $tagId => $checked) {
                $tag = $this->tagService->getTag($tagId);
                if ((bool) $checked === true) {
                    if (!$facility->getTags()->contains($tag)) {
                        $facility->getTags()->add($tag);
                    }
                } else {
                    $key = $facility->getTags()->indexOf($tag);
                    if ($key) {
                        $facility->getTags()->remove($key);
                    }
                }
            }

            $this->facilityService->save($facility);

            $this->redirect()->toRoute('administration');
        }

        return new ViewModel([
            'facility' => $facility,
            'tagGroups' => $tagGroups,
        ]);
    }

    public function facilitiesDeleteAction(): Response
    {
        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId);

        $this->facilityService->delete($facility);

        $this->redirect()->toRoute('administration');
    }

    //--------------------------------------------------------------------------------------------------
    // Contacts
    //--------------------------------------------------------------------------------------------------

    public function contactsAction(): ViewModel
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId);

        return new ViewModel([
            'facility' => $facility
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function contactsAddAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');

            $contact = new Contact($facility);
            $contact
                ->setFirstname($formData['firstname'])
                ->setLastname($formData['lastname'])
                ->setPhoneNumber($formData['phone'])
                ->setMobilenumber($formData['mobile'])
                ->setFaxnumber($formData['fax'])
                ->setEmail($formData['email'])
                ->setPosition($formData['position']);
            $facility->addContact($contact);

            $this->facilityService->save($facility);

            $this->redirect()->toRoute('administration/facilities/contacts', [
                'facilityId' => $facility->getId(),
            ]);
        }

        return new ViewModel([
            'facility' => $facility,
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function contactsEditAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId);

        $contactId = $this->params()->fromRoute('contactId');
        $contact = $this->contactService->getContact((int) $contactId);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');

            $contact
                ->setFirstname($formData['firstname'])
                ->setLastname($formData['lastname'])
                ->setPhoneNumber($formData['phone'])
                ->setMobilenumber($formData['mobile'])
                ->setFaxnumber($formData['fax'])
                ->setEmail($formData['email'])
                ->setPosition($formData['position'])
                ->setUpdatedAt(new \DateTime('now', new \DateTimeZone('utc')));

            $this->contactService->save($contact);

            $this->redirect()->toRoute('administration/facilities/contacts', [
                'facilityId' => $facility->getId(),
            ]);
        }

        return new ViewModel([
            'contact' => $contact,
            'facility' => $facility,
        ]);
    }

    public function contactsDeleteAction(): Response
    {
        $facilityId = $this->params()->fromRoute('facilityId');
        $facility = $this->facilityService->getFacility((int) $facilityId);

        $contactId = $this->params()->fromRoute('contactId');
        $contact = $this->contactService->getContact((int) $contactId);

        $this->contactService->delete($contact);

        $this->redirect()->toRoute('administration/facilities/contacts', [
            'facilityId' => $facility->getId(),
        ]);
    }

    //--------------------------------------------------------------------------------------------------
    // TagGroups
    //--------------------------------------------------------------------------------------------------

    public function taggroupsAction(): ViewModel
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $tagGroups = $this->tagGroupService->getTagGroups();

        return new ViewModel([
            'tagGroups' => $tagGroups,
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function taggroupsAddAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            /** @var TagGroup $tagGroup */
            $tagGroup = new TagGroup();

            $formData = $request->getPost('data');
            $tagGroup->setName($formData['name']);

            $this->tagGroupService->save($tagGroup);

            $this->redirect()->toRoute('administration/taggroups');
        }

        return new ViewModel();
    }

    /**
     * @return ViewModel|Response
     */
    public function taggroupsEditAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $tagGroupId = $this->params()->fromRoute('tagGroupId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');
            $tagGroup->setName($formData['name']);

            $this->tagGroupService->save($tagGroup);

            $this->redirect()->toRoute(
                'administration/taggroups',
                [
                    'tagGroupId' => $tagGroup->getId(),
                ]
            );
        }

        return new ViewModel([
            'tagGroup' => $tagGroup,
        ]);
    }

    public function taggroupsDeleteAction(): Response
    {
        $tagGroupId = $this->params()->fromRoute('tagGroupId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);

        $this->tagGroupService->delete($tagGroup);

        $this->redirect()->toRoute('administration/taggroups');
    }

    //--------------------------------------------------------------------------------------------------
    // Tags
    //--------------------------------------------------------------------------------------------------

    public function tagsAction(): ViewModel
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $tagGroupId = $this->params()->fromRoute('tagGroupId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);

        return new ViewModel([
            'tagGroup' => $tagGroup,
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function tagsAddAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $tagGroupId = $this->params()->fromRoute('tagGroupId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $tag = new Tag();

            $formData = $request->getPost('data');
            $tag->setName($formData['name']);
            $tag->setTagGroup($tagGroup);

            $this->tagService->save($tag);

            $this->redirect()->toRoute(
                'administration/taggroups/tags',
                [
                    'tagGroupId' => $tagGroup->getId(),
                ]
            );
        }

        return new ViewModel([
            'tagGroup' => $tagGroup
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function tagsEditAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/administration');

        $tagGroupId = $this->params()->fromRoute('tagGroupId');
        $tagId = $this->params()->fromRoute('tagId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);
        $tag = $this->tagService->getTag((int) $tagId);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost('data');
            $tag->setName($formData['name']);

            $this->tagService->save($tag);

            $this->redirect()->toRoute(
                'administration/taggroups/tags',
                [
                    'tagGroupId' => $tagGroup->getId(),
                ]
            );
        }

        return new ViewModel([
            'tagGroup' => $tagGroup,
            'tag' => $tag,
        ]);
    }

    public function tagsDeleteAction(): Response
    {
        $tagGroupId = $this->params()->fromRoute('tagGroupId');
        $tagId = $this->params()->fromRoute('tagId');

        /** @var TagGroup $tagGroups */
        $tagGroup = $this->tagGroupService->getTagGroup((int) $tagGroupId);
        $tag = $this->tagService->getTag((int) $tagId);

        $this->tagService->delete($tag);

        $this->redirect()->toRoute('administration/taggroups/tags', [
            'tagGroupId' => $tagGroup->getId(),
        ]);
    }
}