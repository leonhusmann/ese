<?php

declare(strict_types=1);

namespace Administration\Controller;

use Application\Service\ContactService;
use Application\Service\FacilityService;
use Application\Service\OpeningTimesService;
use Application\Service\SearchService;
use Application\Service\TagGroupService;
use Application\Service\TagService;
use Laminas\Authentication\AuthenticationService;
use Psr\Container\ContainerInterface;

class AdministrationControllerFactory
{
    public function __invoke(ContainerInterface $container): AdministrationController
    {
        return new AdministrationController(
            $container->get(AuthenticationService::class),
            $container->get(SearchService::class),
            $container->get(TagGroupService::class),
            $container->get(TagService::class),
            $container->get(FacilityService::class),
            $container->get(ContactService::class),
            $container->get(OpeningTimesService::class)
        );
    }
}