<?php

declare(strict_types=1);

namespace Administration\Service;

use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Storage\Session;
use Laminas\Session\SessionManager;
use Psr\Container\ContainerInterface;

class AuthenticationServiceFactory
{
    public function __invoke(ContainerInterface $container): AuthenticationService
    {
        $sessionManager = $container->get(SessionManager::class);
        $authStorage = new Session('Zend_Auth', 'backendSession', $sessionManager);

        $authAdapter = $container->get(AuthAdapter::class);

        return new AuthenticationService($authStorage, $authAdapter);
    }
}